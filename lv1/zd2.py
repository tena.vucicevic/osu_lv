try:
    num = float(input('Unesite ocjenu: '))
    if num>1.0 or num<0.0:
        print('Unesite ocjenu 0.0-1.0')
    elif num >= 0.9:
        print('A')
    elif num >= 0.8:
        print('B')
    elif num >= 0.7:
        print('C')
    elif num >= 0.6:
        print('D')
    elif num < 0.6:
        print('F')
except ValueError: 
       print('Unesite ocjenu!')
       exit()

