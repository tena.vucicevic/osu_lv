num = []
def calculator(num):
    print('Korisnik je unio', len(num), 'brojeva.')

    min = num[0]
    max = num[0]
    avg = 0
    for number in num:
        avg = avg + number
        if number < min:
            min = number
        if number > max:
            max = number

    avg = avg/len(num)
    print('Srednja vrijednost:', avg)
    print('Minimalna vrijednost:', min)
    print('Maksimalna vrijednost:', max)

    num.sort()
    print(num)
    
while True:
    new = input()
    if new=="Done":
        calculator(num)
        exit()
    try:
        new = int(new)
    except:
        print("Unesi broj!")
        continue
    num.append(new)
