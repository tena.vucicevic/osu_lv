song = open('song.txt')
word_count = {}

for line in song:
     line = line.rstrip()
     words = line.split()
     for word in words:
        if word in word_count:
            word_count[word] += 1
        else:
            word_count[word] = 1

word_count_is_1 = 0
for word in word_count:
    if word_count[word] == 1:
        word_count_is_1 += 1
        print(word)

print(word_count_is_1, 'rijeci se pojavljuju samo jednom.')       
song.close()

