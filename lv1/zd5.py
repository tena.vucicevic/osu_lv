texts = open('SMSSpamCollection.txt')
spam_count=0
spam_lenght=0
usklicnik=0
ham_count=0
ham_lenght=0

for line in texts:
    line = line.rstrip()
    words = line.split()
    if words[0] == "spam":
        spam_count += 1
        spam_lenght += len(words)-1
        if(words[-1].endswith("!")):
            usklicnik += 1
    if words[0] == "ham":
        ham_count += 1
        ham_lenght += len(words)-1

print('Prosjecan broj rijeci u spam:', int(spam_lenght/spam_count), '\nProsjecan broj rijeci u ham:', int(ham_lenght/ham_count), '\nBroj rijeci u spam koje zavrsavaju s usklicnikom:', usklicnik)

texts.close()

