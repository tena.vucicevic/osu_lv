import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('data.csv', skiprows=1, delimiter=',')
print('Mjerenja su izvrsena nad', int(data.size/3), 'osoba.')
height=data[0:len(data), 1]
weight=data[0:len(data), 2]
#plt.scatter(height, weight, marker=".", linewidths=1) - 2.4.2 b)
plt.scatter(height[0::50], weight[0::50], marker=".", linewidths=1)
plt.xlabel('Height')
plt.ylabel('Weight')
plt.title('Height/Weight')
plt.show()
print('Minimalna visina:', height.min())
print('Maksimalna visina:', height.max())
print('Srednja visina:', height.mean())
ind = (data[:,0] == 0)
numberOfMales = 0
for i in ind:
    if(i):
        numberOfMales += 1
print('Minimalna muska visina:', data[:numberOfMales,1].min(), 'Maksimalna muska visina:', data[:numberOfMales,1].max(), 'Srednja muska visina:', data[:numberOfMales,1].mean())
print('Minimalna zenska visina:', data[:numberOfMales,1].min(), 'Maksimalna zenska visina:', data[:numberOfMales,1].max(), 'Srednja zenska visina:', data[:numberOfMales,1].mean())
