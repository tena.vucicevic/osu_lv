import numpy as np
import matplotlib.pyplot as plt
img = plt.imread("road.jpg")
img = img[:,:,0].copy()
brightendImg = img + 80*np.ones(img.shape)
plt.figure()
plt.imshow(brightendImg, cmap="gray", vmin=0, vmax=255)
plt.show()

imgQuarter = int(img.shape[1]/4)
cutImg = img[:,imgQuarter:2*imgQuarter]
plt.figure()
plt.imshow(cutImg, cmap="gray")
plt.show()

rotataedImg = np.rot90(np.rot90(np.rot90(img)))
plt.figure()
plt.imshow(rotataedImg, cmap="gray")
plt.show()

flippedImg = np.fliplr(img)
plt.figure()
plt.imshow(flippedImg, cmap="gray")
plt.show()


