import numpy as np
import matplotlib.pyplot as plt

black = np.zeros((50, 50))
white = np.ones((50, 50))
white = 255*white
halfOne = np.hstack((black, white))
halfTwo = np.hstack((white, black))
image = np.vstack((halfOne, halfTwo))
plt.figure()
plt.imshow(image, cmap='gray')
plt.show()
