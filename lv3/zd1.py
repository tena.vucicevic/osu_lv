import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('lv3/data_C02_emission.csv')
#a
print('Broj mjerenja:', len(data))
print(data.info())
if data.isnull().sum().sum() <= 0 and data.duplicated().sum() <= 0:
    print('Nema izostalih/dupliciranih vrijednosti')
else:
    print('Ima izostalih/dupliciranih vrijednosti')
    data.dropna()
    data.drop_duplicates()
    data.reset_index()
data["Make"] = data["Make"].astype("category")
data["Model"] = data["Model"].astype("category")
data["Vehicle Class"] = data["Vehicle Class"].astype("category")
data["Transmission"] = data["Transmission"].astype("category")
data["Fuel Type"] = data["Fuel Type"].astype("category")

#b
smallest = data.nsmallest(3, ['Fuel Consumption City (L/100km)'])
largest = data.nlargest(3, ['Fuel Consumption City (L/100km)'])

print(smallest[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print(largest[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#c
motor_size= data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] < 3.5)]
print('Broj vozila koji imaju velicinu motora izmedu 2.5 i 3.5 L:', len(motor_size))
print('Prosjecna emisija C02:', motor_size['CO2 Emissions (g/km)'].mean())

#d
audi = data[data['Make'] == 'Audi']
print('Broj vozila marke Audi:', len(audi))
audi_4_cylinders = audi[audi['Cylinders'] == 4]
print('Prosjecna emisija C02 marke Audi s 4 cilindra:', audi_4_cylinders['CO2 Emissions (g/km)'].mean())

#f
dizel = data[data['Fuel Type'] == 'D']
print('Prosjecna gradska potrosnja dizel automobila:', dizel['Fuel Consumption City (L/100km)'].mean())
benzin = data[data['Fuel Type'] == 'X']
print('Prosjecna gradska potrosnja benzin automobila:', benzin['Fuel Consumption City (L/100km)'].mean())
print('dizel medijan:', dizel['Fuel Consumption City (L/100km)'].median(), '\nbenzin medijan:', benzin['Fuel Consumption City (L/100km)'].median())

#g
vozila = data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')]
vozilo = vozila.sort_values(by=['Fuel Consumption City (L/100km)']).tail(1)
print('Vozilo s 4 cilindra i dizelskim motorom koje ima najvecu gradsku potrosnju:', vozilo[['Make', 'Model']])

#h
man = data['Transmission'].apply(lambda x:x.__contains__('M'))
manual = data[man]
print(f"Broj automobila s rucnim mjenjacem: {len(manual)}")

#i
print(data.corr(numeric_only=True))
