from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error, r2_score
import sklearn.linear_model as lm
import pandas as pd
import matplotlib.pyplot as plt
from math import sqrt

data = pd.read_csv('data_C02_emission.csv')

data.drop(['Make', 'Model'], axis=1)
#a
x_data = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]
y_data = data['CO2 Emissions (g/km)']


x_train , x_test , y_train , y_test = train_test_split (x_data , y_data , test_size = 0.2, random_state = 1)

#b
plt.figure()

plt.scatter(x=x_train['Fuel Consumption City (L/100km)'], y=y_train, color='blue', s=5)
plt.scatter(x=x_test['Fuel Consumption City (L/100km)'], y=y_test, color='red', s=5)
plt.xlabel = 'Fuel Consumption City (L/100km)'
plt.ylabel = 'CO2 Emissions(g/km)'

plt.show()

#c
stdSc = StandardScaler()
x_train_std = stdSc.fit_transform(x_train)
column_name = ['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']
x_train_n = pd.DataFrame(x_train_std, columns=column_name)
x_test_std = stdSc.fit_transform(x_test)
column_names = ['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']
x_test_n = pd.DataFrame(x_test_std, columns=column_name)
fig, ax = plt.subplots(2)
ax[0].hist(x_train['Fuel Consumption City (L/100km)'])
ax[0].set_title('Not scaled')
ax[1].hist(x_train_n['Fuel Consumption City (L/100km)'])
ax[1].set_title('Scaled')
plt.show()

#d
linearModel = lm.LinearRegression ()
linearModel.fit(x_train_n , y_train)
print(f"Parametri: \n{linearModel.coef_}")

#e
y_test_p = linearModel.predict(x_test_n)
plt.figure()

plt.scatter(x=x_test['Fuel Consumption City (L/100km)'], y=y_test_p, color='blue', s=5)
plt.scatter(x=x_test['Fuel Consumption City (L/100km)'], y=y_test, color='red', s=5)
plt.xlabel = 'Fuel Consumption City (L/100km)'
plt.ylabel = 'CO2 Emissions(g/km)'

plt.show()

#f
print(f"MSE: {mean_squared_error(y_test, y_test_p)}")
print(f"RMSE: {sqrt(mean_squared_error(y_test, y_test_p))}")
print(f"MAE: {mean_absolute_error(y_test, y_test_p)}")
print(f"MAPE: {mean_absolute_percentage_error(y_test, y_test_p)}")
print(f"R2: {r2_score(y_test, y_test_p)}")  

#g
