from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error, r2_score
import sklearn.linear_model as lm
import pandas as pd
import matplotlib.pyplot as plt
from math import sqrt

data = pd.read_csv('data_C02_emission.csv')
data = data.drop(['Make', 'Model'], axis=1)
categorical_columns = data.select_dtypes(include=['object']).columns.tolist()
encoder = OneHotEncoder(sparse_output=False)
one_hot_encoded = encoder.fit_transform(data[categorical_columns])
one_hot_data = pd.DataFrame(one_hot_encoded, columns=encoder.get_feature_names_out(categorical_columns))
data_encoded = pd.concat([data, one_hot_data], axis=1)

# Drop the original categorical columns
data_encoded = data_encoded.drop(categorical_columns, axis=1)
for col in data_encoded.columns:
    print(col)


x_data = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)', 'Fuel Type']]
y_data = data['CO2 Emissions (g/km)']



x_train , x_test , y_train , y_test = train_test_split (x_data, y_data , test_size = 0.2, random_state = 1)

linearModel = lm.LinearRegression()
linearModel.fit(x_train, y_train)
print(f"Parametri: \n{linearModel.coef_}")
y_test_p = linearModel.predict(x_test)

print(f"MSE: {mean_squared_error(y_test, y_test_p)}")
print(f"RMSE: {sqrt(mean_squared_error(y_test, y_test_p))}")
print(f"MAE: {mean_absolute_error(y_test, y_test_p)}")
print(f"MAPE: {mean_absolute_percentage_error(y_test, y_test_p)}")
print(f"R2: {r2_score(y_test, y_test_p)}")  
