import numpy as np
import matplotlib
import matplotlib.pyplot as plt


from sklearn.datasets import make_classification
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix, precision_score, recall_score, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a)
plt.scatter(X_test[:,0], X_test[:,1], label="Test", cmap="magma", c=y_test, s=10)
plt.scatter(X_train[:,0], X_train[:,1], label="Trening", cmap="plasma", c=y_train, s=10)
plt.show()

#b)
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)

#c)
intercept = LogRegression_model.intercept_[0]
coef1 = LogRegression_model.coef_[0][0]
coef2 = LogRegression_model.coef_[0][1]
a = -(coef2/coef1)
b = -(intercept/coef1)
def decision_boundary(x):
    return a*x + b
fig, ax = plt.subplots()
plt.scatter(X_train[:,0], X_train[:,1], label="Trening", cmap="viridis", c=y_train, s=10)
ax.plot(X_train[:,0], decision_boundary(X_train[:,0]), c="r")
plt.show()

#d)
y_test_p = LogRegression_model.predict(X_test)
cm = confusion_matrix(y_test, y_test_p)
disp = ConfusionMatrixDisplay(cm)
disp.plot ()
plt.show ()

# tocnost
print ("Tocnost :" , accuracy_score(y_test, y_test_p))
# preciznost
print("Preciznost :" , precision_score(y_test, y_test_p))
# odziv
print("Odziv :" , recall_score(y_test, y_test_p))

#e)
for i in range(len(y_test)):
    if y_test[i] == y_test_p[i]:
        plt.scatter(X_test[i, 0], X_test[i, 1], c="lime", s=10)
    else:
        plt.scatter(X_test[i, 0], X_test[i, 1], c="k", s=10)
plt.show()

