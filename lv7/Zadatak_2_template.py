import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_6.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

#1 106,276 boja 


#2
km = KMeans(n_clusters = 2, n_init = 5)
km.fit(img_array_aprox)
labels = km.predict(img_array_aprox)

#3
centers = km.cluster_centers_[labels]
img_recolored = centers.reshape(img.shape)
#4
fig, ax = plt.subplots(1, 2)
ax[0].imshow(img)
ax[0].set_title('Original Image', size=5)
ax[1].imshow(img_recolored)
ax[1].set_title('5-color Image', size=5)
plt.show()

#6
inertia = []
K = range(1,10)
for k in K:
    km = KMeans(n_clusters=k, n_init = 5)
    km = km.fit(img_array_aprox)
    inertia.append(km.inertia_)
    

plt.plot(K, inertia, 'bx-')
plt.xlabel('k')
plt.ylabel('Sum_of_squared_distances')
plt.title('Elbow Method For Optimal k')
plt.show()
#optimalan broj je 2 ?

#7

